<?php
    @ini_set('display_errors', 'on');
    session_start();
    $dir = $_SESSION['dir'];
    $user = $_SESSION['user'];
    $free = $_SESSION['free'];
    if (isset($_FILES['screenshot']) && $_FILES['screenshot']['error'] == 0){
        if (($_FILES['screenshot']['size']/1000000) <= $free){
            $fileInfo = pathinfo($_FILES['screenshot']['name']);
                move_uploaded_file($_FILES['screenshot']['tmp_name'], $dir ."/". basename($_FILES['screenshot']['name']));
                header("Location: home.php");
        }
        else{
            header("Location: home.php");
        }
    }

?>