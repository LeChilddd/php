<?php
require_once('database.php');

$db = getPdo();

    function check($type, $value, $db){
        $typeCol = "SELECT $type FROM users";
        $showType = $db->prepare($typeCol);
        $showType->execute();
        $bddTypes = $showType -> fetchAll();
        foreach ($bddTypes as $bddType){
            if ($bddType[$type] == $value){
                global $check;
                $check++;
                return;
            }
        }
    }
    function addUser($db, $email, $pseudo, $password, $directory){
        $addUser = 'INSERT INTO `users`(email, pseudo, pass, directory) VALUES (:email, :pseudo, :pass, :directory)';
        $insertUser = $db->prepare($addUser);
        $insertUser->execute([
            'email' => $email,
            'pseudo' => $pseudo,
            'pass' => $password,
            'directory' => $directory,
        ]);
    }
    function getUserBookmarks($db){
        $user = $_SESSION['user'];
        $sql = "SELECT user_id FROM users WHERE users.pseudo LIKE ?";
        $query = $db->prepare($sql);
        $query->execute([
            $user
        ]);
        $userId = $query->fetchColumn();
        $sql = "SELECT bookmark, directory FROM bookmarks WHERE bookmarks.user_id LIKE ?";
        $query = $db->prepare($sql);
        $query->execute([
            (int)$userId
        ]);
        $bookmarks = $query->fetchAll();
        return $bookmarks;
    }

?>