<?php
			session_start();
			require_once 'function.php';
			// @ini_set('display_errors', 'on');

			$user = $_SESSION['user'];
			if (isset($_SESSION['dir'])) {
				$dir = $_SESSION['dir'];
				$prevDir = explode("/", $dir);
				$prevDir = array_slice($prevDir, 0, count($prevDir)-1);
				$prevDir = implode("/",$prevDir);
				$files = scandir($dir);
				$fileNumber = count($files);
			}
			else{
				$dir = "/";
				$prevDir = $dir;
				$files = scandir($dir);
				$fileNumber = count($files);
			}
			$filesArray = [];
			for ($i = 2; $i < $fileNumber; $i++){
				$name = $files[$i];
				$size = filesize($dir.'/'.$files[$i]);
				$date = date("m/d/y H:i:s", filemtime($dir.'/'.$files[$i]));
				$type = filetype($dir.'/'.$files[$i]);
				if($type == 'file'){
					$type = pathinfo($dir.'/'.$files[$i], PATHINFO_EXTENSION);
				}
				$filesArray[] = [
					"name" => $name,
					"size" => $size,
					"date" => $date,
					"type" => $type
				];
			}
			function nameCmpAsc($a, $b){
				return strcmp($a[$_GET['col']], $b[$_GET['col']]);
			}
			function nameCmpDesc($a, $b){
				return strcmp($b[$_GET['col']], $a[$_GET['col']]);
			}

			function dateCmpAsc($a, $b){
				return $a[$_GET['col']] > $b[$_GET['col']];
			}
			function dateCmpDesc($a, $b){
				return $b[$_GET['col']] > $a[$_GET['col']];
			}

			function sizeCmpAsc($a, $b){
				return $a[$_GET['col']] > $b[$_GET['col']];
			}
			function sizeCmpDesc($a, $b){
				return $b[$_GET['col']] > $a[$_GET['col']];
			}
			$function = "";
			if ($_GET['order'] === 'asc'){
				switch($_GET['col']){
					case 'name':
						$function = "nameCmpAsc";
						break;
					case 'date':
						$function = "dateCmpAsc";
						break;
					case 'size':
						$function = "sizeCmpAsc";
						break;
				}
			}
			else{
				switch($_GET['col']){
					case 'name':
						$function = "nameCmpDesc";
						break;
					case 'date':
						$function = "dateCmpDesc";
						break;
					case 'size':
						$function = "sizeCmpDesc";
						break;
				}
			}
			usort($filesArray, $function);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Explore your files</title>
	<link rel="stylesheet" href="folder.css">
	<link rel="stylesheet" href="modal.css">
</head>
<body>
	<section id="files">


		<table>
			<thead>
				<tr>
					<th><a href='redir.php?dir=<?php echo 'back';?>'><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FFFFFF"><path d="M0 0h24v24H0z" fill="none"/><path d="M11.67 3.87L9.9 2.1 0 12l9.9 9.9 1.77-1.77L3.54 12z"/></svg></a></th>
					<th class= <?php echo $_GET['col']=='name' ? $_GET['order']=='asc' ? 'desc' : 'asc' : '';?> ><a href='?dir=<?php echo $dir;?>&col=name&order=<?php echo $_GET['col']=='name' && $_GET['order']=='asc' ? 'desc' : 'asc';?>'>Name</a></th>
					<th class= <?php echo $_GET['col']=='date' ? $_GET['order']=='asc' ? 'desc' : 'asc' : '';?> ><a href='?dir=<?php echo $dir;?>&col=date&order=<?php echo $_GET['col']=='date' && $_GET['order']=='asc' ? 'desc' : 'asc';?>'>Date</a></th>
					<th class= <?php echo $_GET['col']=='size' ? $_GET['order']=='asc' ? 'desc' : 'asc': '';?> ><a href='?dir=<?php echo $dir;?>&col=size&order=<?php echo $_GET['col']=='size' && $_GET['order']=='asc' ? 'desc' : 'asc';?>'>Size</a></th>
				</tr>
			</thead>
			<tbody>
<?php
			foreach($filesArray as $fileArray){
				if(in_array($fileArray['type'], ['dir', 'link'] )){
?>
                    <tr class = "linkFolder" data-link="redir.php?dir=<?php echo $dir."/".$fileArray['name'];?>&user=<?php echo $user;?>" oncontextmenu="myFunction(event)">
						<td class = "type"><a><svg xmlns='http://www.w3.org/2000/svg' height='30px' viewBox='0 0 24 24' width='30px' fill='#000000'><path d='M0 0h24v24H0V0z' fill='none'/><path d='M9.17 6l2 2H20v10H4V6h5.17M10 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2h-8l-2-2z'/></svg></a>
                    	<td class = "name"><a class = "folder"><?php echo $fileArray['name'];?></a></td>
                    	<td class = "date"><a><?php echo $fileArray['date'];?></a></td>
                    	<td class = "size"><a><?php echo $fileArray['size'];?></a></td>
                    </tr>
<?php
                }
                else if(in_array($fileArray['type'], ['jpg', 'png', 'jpeg', 'gif'] )){
?>
					<tr class = "linkImage" data-link="viewfile.php?img=<?php echo $dir."/".$fileArray['name'];?>" oncontextmenu="myFunction(event)">
						<td class = "type"><svg xmlns="http://www.w3.org/2000/svg" height="30px" viewBox="0 0 24 24" width="30px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M19 5v14H5V5h14m0-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-4.86 8.86l-3 3.87L9 13.14 6 17h12l-3.86-5.14z"/></svg></a>
						<td class = "name"><a class = "file" ><?php echo $fileArray['name'];?></a></td>
						<td class = "date"><a><?php echo $fileArray['date'];?></a></td>
						<td class = "size"><a><?php echo $fileArray['size'];?></a></td>
					</tr>
<?php
                }
                else{
?>
					<tr class = "linkFile" data-link="viewfile.php?file=<?php echo $dir."/".$fileArray['name'];?>" oncontextmenu="myFunction(event)">
						<td class = "type"><svg xmlns="http://www.w3.org/2000/svg" height="30px" viewBox="0 0 24 24" width="30px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M8 16h8v2H8zm0-4h8v2H8zm6-10H6c-1.1 0-2 .9-2 2v16c0 1.1.89 2 1.99 2H18c1.1 0 2-.9 2-2V8l-6-6zm4 18H6V4h7v5h5v11z"/></svg></a>
						<td class = "name"><a class = "file" ><?php echo $fileArray['name'];?></a></td>
						<td class = "date"><a><?php echo $fileArray['date'];?></a></td>
						<td class = "size"><a><?php echo $fileArray['size'];?></a></td>
					</tr>
<?php
				}
			}
?>
			</tbody>
			</table>
			<svg id="add" xmlns="http://www.w3.org/2000/svg" height="36px" viewBox="0 0 24 24" width="36px" fill="#FFFFFF"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/></svg>
	</section>
	<section id="menu" class="">
		<h2 id="user">
<?php
		echo $user;

		
?>
		</h2>
<?php
		    $iterator = new RecursiveIteratorIterator(
				new RecursiveDirectoryIterator("/users/$user")
			);
			 
			$totalSize = 0;
			foreach($iterator as $file) {
				$totalSize += $file -> getSize();
			}
			$total = 500;
			$taken = $totalSize/1000000;
			$_SESSION['free'] = $total - $taken;
			$free = $_SESSION['free'];
			$taken = $taken / $total * 100;
?>
		<h3>Place Libre</h3>
		<div class="size">
			<h4 id="size-free"><?php echo $free ?> Mo </h4>
			<h4 id="size-total"><?php echo $total ?> Mo </h4>
			<h4 id="size-taken"><?php echo $taken ?> </h4>
			<svg  xmlns="http://www.w3.org/2000/svg" height="200" width="200" viewBox="0 0 200 200">
				<path d="M41 149.5a77 77 0 1 1 117.93 0"
					class="bg" 
					stroke="#CFCFCF" 
					fill="none"
					/>
				<path d="M41 149.5a77 77 0 1 1 117.93 0"
					class="bar" 
					fill="none"
					/>
			</svg>
		</div>
<?php
			$bookmarks = getUserBookmarks();
			foreach($bookmarks as $bookmark){
?>
				<h3 class = "linkFolder" data-link="redir.php?dir=<?php echo $bookmark['directory'];?>&user=<?php echo $user;?>"><?php echo $bookmark['bookmark'];?></h3>
<?php
			}
?>
		
	</section>

	<div id="contextMenu" class="context-menu" style="display: none"> 
			<button class="properties-btn" data-type ="add" >Add</button>
			<button class="properties-btn" data-type ="delete" >Delete</button>
			<button class="properties-btn" data-type ="fav">Add to Fav</button>
			<button class="properties-btn" data-type ="removefav">del from Fav</button>
			<button class="properties-btn" data-type ="rename">Rename</button>
		</form>
    </div> 


<div id="myModal" class="modal hidden">

  <div class="modal-content">
    <span class="close">&times;</span>
    <p></p>
	<form action="" method="POST" id="modFiles">
		<button type="button" class="refuse-btn">No</button>
		<button type="submit" class="validate-btn" name="action" value="">Yes</button>
	</form>
  </div>

</div>
<div id="addModal" class="modal hidden">

	<div class="addModal-content">
		<span class="close">&times;</span>
		<div class="change-action-btn">
			<button class="addFolder-btn">add folder</button>
			<button class="upload-btn">upload</button>
		</div>
		<div class="addFolder active">
		<form action="addFolder.php" method="POST" id="addFolder">
				<input placeholder="Folder Name" type="text" id="changeName" name="changeName"/>
				<button type="submit" class="btn" name="action" value="addFolder">+</button>
		</form>
		</div>
		<div class="upload hidden">
			<form action="upload.php?dir=<?php echo "$dir"?>" method="POST" enctype="multipart/form-data">
				<input type="file" class="form-control" id="screenshot" name="screenshot" />
				<button type="submit" class="btn btn-primary">Upload</button>
			</form>
			</div>
	</div>
</div>
<script src="index.js"></script>
<script src="modal.js"></script>
</body>
</html>
