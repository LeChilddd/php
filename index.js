var folders = document.querySelectorAll(".linkFolder")
var images = document.querySelectorAll(".linkImage")
var files = document.querySelectorAll(".linkFile")
var pros = document.querySelectorAll('.properties')

document.onclick = hideMenu; 

    function hideMenu() { 
        document.getElementById("contextMenu").style.display = "none" 
    } 

    function myFunction(e) { 
        e.preventDefault(); 
        if (document.getElementById("contextMenu") .style.display == "block"){ 
            hideMenu(); 
        }else{ 
            var menu = document.getElementById("contextMenu")     
            menu.style.position = "absolute";
            menu.style.zindex = "2"; 
            menu.style.display = 'flex'; 
            menu.style.left = e.pageX + "px"; 
            menu.style.top = e.pageY + "px";
            elemenType = e.target.parentElement.querySelector('.name a').classList[0]
            elementName = e.target.parentElement.querySelector('.name').innerText
        }
    } 

folders.forEach(folder => {
    folder.addEventListener('dblclick', event =>{
        var link = folder.dataset.link
        window.location.href = link;
    })
})


images.forEach(image => {
    image.addEventListener('dblclick', event =>{
        var link = image.dataset.link
        window.location.href = link;
    })
})

files.forEach(file => {
    file.addEventListener('dblclick', event =>{
        var link = file.dataset.link
        window.location.href = link;
    })
})


const bar = document.querySelector('.bar');
const bg = document.querySelector('.bg');
const btn = document.querySelector('#start');
const control = document.getElementById('control');
var value = document.querySelector("#size-taken")
value = value.innerHTML

let length = 350;

let progress = value / 100;
let dashoffset = length * (1 - progress);
  
bar.style.strokeDashoffset = dashoffset;

bar.style.strokeDasharray = length;

