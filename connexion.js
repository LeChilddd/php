var btn = document.querySelector("#change-form-type")
var formType = document.querySelector("#form-type")
const logForm = document.querySelector('#log-form')
const signForm = document.querySelector('#sign-form')
const log = "Log-In"
const sign = "Sign-In"
const logSvg = document.querySelector('.log-svg')
const signSvg = document.querySelector('.sign-svg')


btn.addEventListener('click', event => {
    if (btn.classList.contains('sign')){
        btn.classList.replace('sign', 'log')
        logSvg.classList.remove('none')
        signSvg.classList.add('none')
        formType.innerHTML = sign
        logForm.classList.replace('active', 'inactive')
        logForm.classList.add('none')
        signForm.classList.remove('none')
        signForm.classList.replace('inactive','active')
    }
    else if (btn.classList.contains('log')){
        btn.classList.replace('log', 'sign')
        logSvg.classList.add('none')
        signSvg.classList.remove('none')
        formType.innerHTML = log
        signForm.classList.replace('active', 'inactive')
        signForm.classList.add('none')
        logForm.classList.remove('none')
        logForm.classList.replace('inactive','active')
    }
})

