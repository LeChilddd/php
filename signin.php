<?php


require_once('database.php');

$db = getPdo();
include 'function.php';
@ini_set('display_errors', 'on');
$type = ['email', 'pseudo'];
$email = $_POST['email'];
$pseudo = $_POST['pseudo'];
$password = $_POST['password'];
$directory = "/users/$pseudo";


    if(isset($email) && isset($pseudo) && isset($password) ){
    $check = 0;
    check($type[0], $email, $db);
    check($type[1], $pseudo, $db);
    if ($check !=0){
        header('Location: index.php?error=1');
    }
    else{
        addUser($db, $email, $pseudo, $password, $directory);
        mkdir($directory, 0777, true);
        mkdir($directory."/Images", 0777, true);
        mkdir($directory."/Documents", 0777, true);
        mkdir($directory."/Vidéos", 0777, true);
        header('Location: index.php?error=0');
    }
}


?>