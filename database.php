<?php

/*
*   Retourne la connexion a la BDD
*/


function getPdo(){
    $db = new PDO('mysql:host=localhost;dbname=explorateur;charset=utf8', 'adminEx', 'test', [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ]);

    return $db;
}