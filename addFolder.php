<?php
    include 'function.php';
    @ini_set('display_errors', 'on');
    session_start();
    require_once('database.php');

    $db = getPdo();


    if ($_POST['action'] === 'addFolder'){
        addFolder();
    }
    else if ($_POST['action'] === 'deleteFolder'){
        $isInBookmark = isInBookmark();
        var_dump($isInBookmark);
        if ($isInBookmark=='true'){
            removeToFav($db);
        }
        deleteFolder();
    }
    else if ($_POST['action'] === 'deleteFile'){
        $isInBookmark = isInBookmark();
        if ($isInBookmark == 'true'){
            removeToFav($db);
        }
        deleteFile();
    }
    else if ($_POST['action'] === 'rename'){
        $isInBookmark = isInBookmark();
        if ($isInBookmark=='true'){
            updateBookmarks($db);
        }
        renameFile();
    }
    else if ($_POST['action'] === 'fav'){
        $totalUserBookmarks = countUserbookmarks($db);
        if ($totalUserBookmarks < 5){
            addToFav($db);
        }
        header("Location: home.php");
    }
    else if ($_POST['action'] === 'removefav'){
        removeToFav($db);
    }
    else {
        header("Location: home.php");
    }
    function addFolder(){
        $folderName = $_POST['changeName'];
        $dir = $_SESSION['dir'];
        if(is_dir($dir."/".$folderName)){
            header("Location: home.php");
        }
        else{
            mkdir($dir."/".$folderName);
        }
        header("Location: home.php");
    }
    function deleteFolder(){
        $folderName = $_GET['name'];
        $dir = $_SESSION['dir'];
        rmdir($dir."/".$folderName);
        header("Location: home.php");
    }

    function renameFile(){
        $dir = $_SESSION['dir'];
        $oldName = $_GET['name'];
        $newName = $_POST['changeName'];
        if (is_dir($dir."/".$oldName)){
            rename($dir."/".$oldName, $dir."/".$newName);
        }else {
            rename($dir."/".$oldName, $dir."/".$newName.".".pathinfo($dir."/".$oldName, PATHINFO_EXTENSION));
        }
        header("Location: home.php");
    }
    function deleteFile(){
        $fileName = $_GET['name'];
        $dir = $_SESSION['dir'];
        unlink($dir."/".$fileName);
        header("Location: home.php");
    }
    function addToFav($db){
        $bookmark = $_GET['name'];
        $dir = $_SESSION['dir'];
        $dir = $dir ."/". $bookmark;
        $user = $_SESSION['user'];
        $sql = "SELECT user_id FROM users WHERE users.pseudo LIKE ?";
        $query = $db->prepare($sql);
        $query->execute([
            $user
        ]);
        $userId = $query->fetchColumn();
        $addFav = 'INSERT INTO `bookmarks`(user_id, bookmark, directory) VALUES (?, ?, ?)';
            $insertFav = $db->prepare($addFav);
            $insertFav->execute([
                (int)$userId,
                $bookmark,
                $dir,
            ]);
        header("Location: home.php?fav=youpi");
    }

    function removeToFav($db){
        $bookmark = $_GET['name'];
        $dir = $_SESSION['dir'];
        $user = $_SESSION['user'];
        $sql = "SELECT user_id FROM users WHERE users.pseudo LIKE ?";
        $query = $db->prepare($sql);
        $query->execute([
            $user
        ]);
        $userId = $query->fetchColumn();
        $sql = "DELETE FROM bookmarks WHERE bookmarks.user_id LIKE ? AND bookmarks.bookmark LIKE ?";
        $query = $db->prepare($sql);
        $query->execute([
            (int)$userId,
            $bookmark
        ]);
        header("Location: home.php?fav=hourra");
    }

    function countUserbookmarks($db){
        $user = $_SESSION['user'];
        $sql = "SELECT user_id FROM users WHERE users.pseudo LIKE ?";
        $query = $db->prepare($sql);
        $query->execute([
            $user
        ]);
        $userId = $query->fetchColumn();
        $sql = "SELECT bookmark FROM bookmarks WHERE bookmarks.user_id LIKE ?";
        $query = $db->prepare($sql);
        $query->execute([
            (int)$userId,
        ]);
        $userBookmarks = $query->fetchAll();
        return count($userBookmarks);
    }

    function isInBookmark(){
        $fileName = $_GET['name'];
        $userBookmarks = getUserBookmarks();
        foreach($userBookmarks as $userBookmark){
            if ($fileName === $userBookmark["bookmark"]){
                return $isInBookmark = 'true';
            }
        }
    }

    function updateBookmarks($db){
        $user = $_SESSION['user'];
        $oldName = $_GET['name'];
        $newName = $_POST['changeName'];
        $dir = $_SESSION['dir'];
        $dir = $dir . "/" . $newName;
        $sql = "SELECT user_id FROM users WHERE users.pseudo LIKE ?";
        $query = $db->prepare($sql);
        $query->execute([
            $user
        ]);
        $userId = $query->fetchColumn();
        $sql = "UPDATE bookmarks SET bookmark = ?, directory = ? WHERE bookmarks.user_id LIKE ? AND bookmarks.bookmark LIKE ?";
        $query = $db->prepare($sql);
        $query->execute([
            $newName,
            $dir,
            (int)$userId,
            $oldName,
        ]);
    }