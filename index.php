<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Connexion</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <section id="all">
        <div class="title">
            <a id="form-type">Log-In</a>
            <button id="change-form-type" class="sign">
                <svg class="sign-svg" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="34px" viewBox="0 0 24 24" width="34px" fill="#FFFFFF"><g><rect fill="none" height="24" width="24"/></g><g><path d="M20,9V6h-2v3h-3v2h3v3h2v-3h3V9H20z M9,12c2.21,0,4-1.79,4-4c0-2.21-1.79-4-4-4S5,5.79,5,8C5,10.21,6.79,12,9,12z M9,6 c1.1,0,2,0.9,2,2c0,1.1-0.9,2-2,2S7,9.1,7,8C7,6.9,7.9,6,9,6z M15.39,14.56C13.71,13.7,11.53,13,9,13c-2.53,0-4.71,0.7-6.39,1.56 C1.61,15.07,1,16.1,1,17.22V20h16v-2.78C17,16.1,16.39,15.07,15.39,14.56z M15,18H3v-0.78c0-0.38,0.2-0.72,0.52-0.88 C4.71,15.73,6.63,15,9,15c2.37,0,4.29,0.73,5.48,1.34C14.8,16.5,15,16.84,15,17.22V18z"/></g></svg>
                <svg class="log-svg none" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="34px" viewBox="0 0 24 24" width="34px" fill="#FFFFFF"><g><rect fill="none" height="24" width="24"/></g><g><path d="M11,7L9.6,8.4l2.6,2.6H2v2h10.2l-2.6,2.6L11,17l5-5L11,7z M20,19h-8v2h8c1.1,0,2-0.9,2-2V5c0-1.1-0.9-2-2-2h-8v2h8V19z"/></g></svg>
            </button>
        </div>
        <form action="login.php" method="POST" id="log-form" class="active form">
                <input placeholder="Email" type="text" class="email" id="email-log" name="email"/>
                <input placeholder="Password" type="password"  class="password" name="password"/>
            <button type="submit" class="btn submit-btn">Log-In</button>
        </form>
        <form action="signin.php" method="POST" id="sign-form" class="inactive form none">
            <input placeholder="Pseudo" type="text" id="pseudo" name="pseudo"/>
            <input placeholder="Email" type="text" class="email" id="email-sign" name="email"/>
            <input placeholder="Password" type="password"  class="password" name="password"/>
            <button type="submit" class="btn submit-btn">Sign-In</button>
        </form>
    </div>
<?php
    if($_GET['error']=='1'){
?>
        <a class="sign-message">Problème d'inscription</a>
<?php
    }
    else if($_GET['error']=='0'){
?>
        <a class="sign-message">Inscription Réussis</a>
<?php
    }
?>
    </section>
    <script src="connexion.js"></script>
</body>
</html>