<!DOCTYPE html>
<html>
<head>
<title>Explore your files</title>
<link rel="stylesheet" href="folder.css">
</head>
<body>
	<section id="files">
		<?php
			$dir = "/etc";
			$files = scandir($dir);
			$fileNumber = count($files);
			echo "<table>";
			echo "<tbody>";
			for ($i = 0; $i < $fileNumber; $i++){
				$name = $files[$i];
				$size = filesize($dir.'/'.$files[$i]);
				$date = date("F d Y", fileatime($files[$i]));
				if(is_dir($dir.'/'.$files[$i])){
                    echo "<tr>";
                    echo "<td class = 'folder'>$name</td>";
                    echo "<td>$size</td>";
                    echo "<td>$date</td>";
                    echo "</tr>";
                }
                else{
                    echo "<tr>";
                    echo "<td class = 'file'>$name</td>";
                    echo "<td>$size</td>";
                    echo "<td>$date</td>";
                    echo "</tr>";
                }
			}
			echo "</tbody>";
			echo "</table>";
			// echo '<pre>' . print_r($allFiles, true) . '</pre>';
		?>
	</section>
<script src="index.js"></script> 
</body>
</html>