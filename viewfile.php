<?php
?>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Connexion</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php
    if (isset($_GET['img'])){
        $file = $_GET['img'];
?>
        <img SRC="data:image/jpeg;base64,<?php echo base64_encode(file_get_contents("$file"));?>">

<?php
    }
    else {

        $file = $_GET['file'];
        $fileExt = pathinfo($file, PATHINFO_EXTENSION);
        switch ($fileExt){
            case 'odt':
                echo extracttext($file);
                break;
            case 'pdf':
                header("Content-type: application/pdf");
                header("Content-Length: " . filesize($file));
                readfile($file);
                break;
            case 'docx':
                header("Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                header("Content-Length: " . filesize($file));
                readfile($file);
                break;
            case 'doc':
                header("Content-type: application/msword");
                header("Content-Length: " . filesize($file));
                readfile($file);
                break;
        }
    }
?>
</body>
</html>