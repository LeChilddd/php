var modalForm = document.querySelector('#modFiles')
var inputName = document.createElement("INPUT");
var elementName = ""
var elemenType = "";
var modal = document.getElementById("myModal");
var modalContent = document.querySelector(".modal-content");
var btns = document.querySelectorAll(".properties-btn");
const p = document.querySelector(".modal-content p")
var spans = document.querySelectorAll(".close");
var noBtn = document.querySelector('.refuse-btn')
var yesBtn = document.querySelector('.validate-btn')


if (document.querySelector("#modFiles input")){
    document.querySelector("#modFiles input").remove()
}
inputName.setAttribute("type", "text")
inputName.setAttribute("id", "changeName")
inputName.setAttribute("name", "changeName")

// When the user clicks on the button, open the modal
btns.forEach(btn => {
    btn.addEventListener('click', event => {
        if(btn.dataset.type=="delete"){
            if(elemenType == 'file'){
            p.innerText = "delete this file"
            yesBtn.value = "deleteFile"
            }else {
                p.innerText = "delete this folder"
                yesBtn.value = "deleteFolder"
            }
            modalForm.action="addFolder.php?name="+elementName
        }
        else if (btn.dataset.type=="fav"){
            p.innerText = "add this in fav"
            yesBtn.value = "fav"
            modalForm.action="addFolder.php?name="+elementName
        }
        else if (btn.dataset.type=="removefav"){
            p.innerText = "remove this from fav"
            yesBtn.value = "removefav"
            modalForm.action="addFolder.php?name="+elementName
        }
        else if (btn.dataset.type=="rename"){
            p.innerText = "rename this file"
            yesBtn.value = "rename"
            inputName.setAttribute("placeholder", elementName)
            modalForm.prepend(inputName);
            modalForm.action="addFolder.php?name="+elementName
        }
        else if (btn.dataset.type=="add"){
            p.innerText = "Choose folder name"
            yesBtn.value = "addFolder"
            inputName.setAttribute("placeholder", "folder name")
            modalForm.prepend(inputName);
            modalForm.action="addFolder.php"
        }
        modal.classList.remove('hidden');
    })
})
// When the user clicks on <span> (x), close the modal
spans.forEach(span => {
    span.addEventListener("click", event => {
        span.parentElement.parentElement.classList.add('hidden');
    })
})
noBtn.onclick = function() {
    modal.classList.add('hidden');
}


// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.classList.add('hidden');
  }
  else if (event.target == addModal) {
    addModal.classList.add('hidden');
  }
}


var addBtn = document.querySelector("#add")
var addModal = document.querySelector("#addModal")
var uploadBtn = document.querySelector(".upload-btn")
var addFolderbtn = document.querySelector(".addFolder-btn")
var addFolderForm = document.querySelector(".addFolder")
var uploadForm = document.querySelector(".upload")

addBtn.addEventListener('click',event => {
    addModal.classList.remove("hidden")
})

uploadBtn.addEventListener('click',event => {
    uploadForm.classList.replace("hidden", "active")
    addFolderForm.classList.add("hidden")
})

addFolderbtn.addEventListener('click',event => {
    uploadForm.classList.add("hidden")
    addFolderForm.classList.replace("hidden", "active")
})


